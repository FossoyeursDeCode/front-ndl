const KEY_GAUCHE = 37;
const KEY_HAUT = KEY_GAUCHE+1;
const KEY_DROITE = KEY_HAUT+1;
const KEY_BAS = KEY_DROITE+1;
const KEY_A = 65;
const KEY_B = KEY_A+1;

var konami = [KEY_HAUT, KEY_HAUT, KEY_BAS, KEY_BAS, KEY_GAUCHE, KEY_DROITE, KEY_GAUCHE, KEY_DROITE, KEY_B, KEY_A];
var sequence = 0;

function random(min, max){
  rand = (Math.random() * max) + min;
  return rand;
}

$(document).keydown(function (e) {
    if (e.keyCode == konami[sequence]){
      sequence+=1;
      if (sequence == konami.length) {
        i=10;
        setInterval(function(){
          rand = random(0, 255)
          $('body').css('background-color', 'rgb('+ rand +','+ rand +','+ rand +')');
          $('#robot-eye-left').css('background','rgb('+ random(0, 255) +','+ random(0, 255) +','+ random(0, 255) +')');
          $('#robot-eye-right').css('background','rgb('+ random(0, 255) +','+ random(0, 255) +','+ random(0, 255) +')');
          $('#robot-antenna-right').css('transform','rotate('+ random(20, 70) + 'deg)');
          $('#robot-antenna-left').css('transform','rotate(-'+ random(20, 70) + 'deg)');
          i+=10;
          if(i >= 500)
            window.location.replace("konami.html");
        }, 1000/i);
        sequence = 0;
    }}
    else
        sequence = 0;
});

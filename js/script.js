/**
 * Created by alexr on 07/12/2017.
 */

const url = '127.0.0.1';
let scene = 0;
const userId = new Date().getUTCMilliseconds();

(function () {
    var Message;
    var page;
    Message = function (arg) {
        this.text = arg.text, this.message_side = arg.message_side;
        this.draw = function (_this) {
            return function () {
                var $message;
                $message = $($('.message_template').clone().html());
                $message.addClass(_this.message_side).find('.text').html(_this.text);
                $('.messages').append($message);
                return setTimeout(function () {
                    return $message.addClass('appeared');
                }, 0);
            };
        }(this);
        return this;
    };
    $(function () {

        let message_side;
        message_side = 'right';
        getMessageText = function () {
            let $message_input;
            $message_input = $('.message_input');
            return $message_input.val();
        };

        function getMessageText(){
          return $('.message_input').val();
        }

        function addMessage(text, message_side = 'right'){
          let $messages, message;
          if (text.trim() === '') {
            return;
          }
          $('.message_input').val('');
          $messages = $('.messages');
          message = new Message({
            text: text,
            message_side: message_side
          });
          message.draw();
          $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
        }


        $('#lol').click(function (e) {
            var a = getMessageText();
            $('.reponse').remove();
            $('.zoneReponse').append("	<div class='reponse'> <div  class='message_input_wrapper'> <input class='message_input' placeholder='Type your message here...' /> </div> <div id='ok' class='send_message'> <div class='icon'></div> <div class='text'>Send</div> </div> </div>");
            //alert("lol");
           return addMessage(a);
        });

        $('#ok').click(function (e) {
            var a = getMessageText();
            //alert("lol");
            $('.reponse').remove();
            $('.zoneReponse').append("<div class='reponse'> <div class='message_input_wrapper'> <input class='message_input' placeholder='Type your message here...' /> </div> <div id='lol' class='send_message'> <div class='icon'></div> <div class='text'>Send</div> </div> </div> ");
            return addMessage(a);
        });


        $('.message_input').keyup(function (e) {
            if (e.which === 13) {
                addMessage(getMessageText());
            }
        });

        $('.send_message').click(function (e) {
          addMessage(getMessageText());
        });

        function getGender(){
          if ($('#homme').is(':checked'))
            return 'male';
          if ($('#femme').is(':checked'))
            return 'female';
          else
            return 'other';
        }

        function ifProba(){
          return $('#probatoire').is(':checked');
        }

        function addUser(){

          $.ajax({
            url:"http://"+url+":3000/user",
            crossDomain: true,
            dataType: 'json',
            method:"POST", //First change type to method here

            data:{
              userName: $('#prenom').val(), // Second add quotes on the value.
              userYear: $('#age').val(),
              userGender: getGender(),
              userNovice: ifProba(),
              userId: userId
            },
            success:function(response) {
              console.log(response);
            },
            error:function(res){
              console.log(res)
            }
          });
        }

        $('#valid_ins').click(function (e) {
          addUser();
          sendMessage();
        });

        function sendMessage(){
          $.ajax({
            url:"http://"+url+":3000/message",
            crossDomain: true,
            dataType: 'json',
            method:"POST", //First change type to method here

            data:{
              message: $('#age').val(),
              userId
            },
            success:function(response) {
              console.log(response);
            },
            error:function(res){
              console.log(res)
            }
          });
        }


/*
        $('.btn1').click(function (e) {
            for(i=0;i<3;i++) {
                $("input:button, a, button", ".boutons_patient").button();
            }




            if (e.which === 13) {
                alert("yo");
            }
        });*/
        addMessage('Hi Philippe ! :)','left');
        setTimeout(function () {
            return addMessage('Hi Sandy! How are you?');
        }, 1000);
        return setTimeout(function () {
            return addMessage('<iframe width="600px" height="280px" src="reactionButton.html"></iframe>');
        }, 2000);
    });
}.call(this));
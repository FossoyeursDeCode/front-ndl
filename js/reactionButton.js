function getRandomColor() {

    var letters = "0123456789ABCDEF".split('');
    var color = "#";
    for (var i = 0; i < 6; i++) {
        color += letters[Math.round(Math.random() * 15)];
    } //ends for loop
    return color;


} // ends getRandomColor Function


var clickedTime; var createdTime; var reactionTime=0;

function makeBox() {
    var time=Math.random()+12;
    time=time*300;

    setTimeout(function() {

        if (Math.random()>0.5) {

            document.getElementById("box").style.borderRadius="100px";

        } else {

            document.getElementById("box").style.borderRadius="100px";
        }

        var top= 0;
        var left= 0;

        document.getElementById("box").style.top = top + "px";
        document.getElementById("box").style.left = left + "px";

        document.getElementById("box").style.backgroundColor=getRandomColor();

        document.getElementById("box").style.display="block";

        createdTime=Date.now();

    }, time);

}

document.getElementById("box").onclick=function() {

    clickedTime=Date.now();

    reactionTime=(clickedTime-createdTime)/1000;    //Donnée de temps par l'utilisateur récupérée
    document.getElementById("printReactionTime").innerHTML="Votre temps de réaction est de " + reactionTime + " seconde(s)";
    if(reactionTime >= 1.5)
    {
        document.getElementById("printReactionTime").innerHTML+="<br>Vous n'êtes pas apte à conduire !";
    }
    else
    {
        document.getElementById("printReactionTime").innerHTML+="<br>Vous êtes apte à conduire !";
    }

    this.style.display="none";

    //makeBox();


}
$(document).ready(function(){
    var canvas = document.getElementById('c-timer');
    var ctx = canvas.getContext('2d');
    var deg = 0;
    var time = 3;
    var radian = 0;
    var checkTime = 0;

    ctx.beginPath();
    ctx.arc(50, 50, 44, 0, Math.PI * 2, false);
    ctx.lineWidth = 6;
    ctx.strokeStyle = "#eee";
    ctx.stroke();

    var animInterval = setInterval(function(){
        animStart();
    }, 31.25);

    var toRadian = function (deg) {
        return deg * (Math.PI / 180);
    };

    var animStart = function () {
        checkTime++;
        if (checkTime == 32) {
            time -= 1;

            ctx.clearRect(10, 10, 150, 150);
            ctx.font = "Bold 50px Arial";
            ctx.textAlign = "center";
            ctx.fillStyle = "#4d4d4d";
            ctx.fillText(time, 50, 65);
            if (time === 0) {
                clearTimeout(animInterval);
                document.getElementById("c-timer2").style.display="none";
                makeBox();
            }
            checkTime = 0;
        }

        deg += 1.120*3.42;
        radian = toRadian(deg);

        ctx.beginPath();
        ctx.arc(50, 50, 44, 0, radian, false);
        ctx.lineWidth=6;
        ctx.strokeStyle="#00a9ff";
        ctx.stroke();
    };

});






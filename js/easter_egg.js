var eyeLeftOn = false;
var eyeRightOn = false;

$('#robot-eye-left').click(function() {
  eyeLeftOn = !eyeLeftOn;
  if(eyeLeftOn){
      $('#robot-eye-left').css('background','#1E90FF');
      $('#robot-antenna-left').css('transform','rotate(-60deg)');
      if(eyeRightOn){
          $('#modalEasterEgg').modal();
      }
  }
  else{
    $('#robot-eye-left').css('background','#FEFEFE');
    $('#robot-antenna-left').css('transform','rotate(-45deg)');
  }
});


$('#robot-eye-right').click(function() {
  eyeRightOn = !eyeRightOn;
  if(eyeRightOn){
      $('#robot-eye-right').css('background','#1E90FF');
      $('#robot-antenna-right').css('transform','rotate(60deg)');
      if(eyeLeftOn){
          $('#modalEasterEgg').modal();
      }
  }
  else{
    $('#robot-eye-right').css('background','#FEFEFE');
    $('#robot-antenna-right').css('transform','rotate(45deg)');
  }
});

$('#modalEasterEgg').on('hide.bs.modal', function() {
  $('#robot-eye-left').css('background','#FEFEFE');
  $('#robot-antenna-left').css('transform','rotate(-45deg)');
  $('#robot-eye-right').css('background','#FEFEFE');
  $('#robot-antenna-right').css('transform','rotate(45deg)');
  eyeLeftOn = false;
  eyeRightOn = false;
});
